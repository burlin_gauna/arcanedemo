﻿using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Sirenix.OdinInspector;
public class GenericCharacter : SerializedMonoBehaviour
{
    [SerializeField]
    protected UnitType _UnitType;

    [SerializeField]
    protected ReactiveProperty<float> _CurrentHealth = new ReactiveProperty<float>();

    [SerializeField]
    protected Vector3 _OriginalPosition;

    [SerializeField]
    protected ActionState _ActionState;

    [SerializeField]
    protected CharacterData _CharData;
    public float CurrentAP { get { return _CurrActionPts.Value; } }

    [SerializeField]
    protected Sprite[] _SpriteArray;

    [SerializeField]
    protected GameObject _TargetObj;

    [SerializeField]
    protected Animator _Animator;

    [SerializeField]
    protected ReactiveProperty<float> _CurrActionPts = new ReactiveProperty<float>();

    [SerializeField]
    protected List<GenericSkill> _Skills;


    public virtual void Awake()
    {
        _CurrActionPts.Value = _CharData.ActionPoints;
        _CurrentHealth.Value = _CharData.Health;
    }

    public void DeductAP(float val)
    {
        _CurrActionPts.Value -= val;
    }
}