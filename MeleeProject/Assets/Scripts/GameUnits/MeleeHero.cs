﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class MeleeHero : GenericCharacter, IDamageable, ITurnable
{
    [SerializeField]
    private Image _HealthImage;
    [SerializeField]
    private Text _ActionPtText;
    [SerializeField]
    private List<TargetData> _TargetObjects;
    [SerializeField]
    private SkillType _ActiveSkill;
    [SerializeField]
    private ISkillable _ActiveSkillable;

    [SerializeField]
    private bool _AI;
    public bool IfAI { get { return _AI; } }

    [SerializeField]
    private float _AccuracyMagnitude;

    public override void Awake()
    {
        base.Awake();

        _CurrActionPts.Subscribe(_ => 
        {
            _ActionPtText.text = _.ToString();
        }).AddTo(this);
        _CurrentHealth.Subscribe(_ =>
        {
            _HealthImage.fillAmount = _ / _CharData.Health;
        }).AddTo(this);

        _OriginalPosition = transform.position;


        MessageBroker.Default.Receive<TriggerSkillSignal>().Subscribe(_ => 
        {
            if(_.SkillData.TargetType == TargetType.Self)
            {
                SelfTarget(_.SkillData.SkillType);
            }
            else if(_.SkillData.TargetType == TargetType.MultipleEnemy)
            {
                UseOffensiveSkill(_.SkillData.SkillType);
            }

        }).AddTo(this);

        MessageBroker.Default.Receive<AttackSignal>().Subscribe(_ =>
        {
            if (_.PlayerObj == gameObject)
            {
                _AccuracyMagnitude = _.Magnitude;
                _TargetObjects = new List<TargetData>();
                int enemyCount = _.EnemyList.Count;
                for (int i = 0; i < enemyCount; i++)
                {
                    _TargetObjects.Add(_.EnemyList[i]);
                }
                AttackEnemy();
            }
        }).AddTo(this);
        MessageBroker.Default.Receive<EnemyTargetSignal>().Subscribe(_ =>
        {
            _TargetObj = _.EnemyObject;
        }).AddTo(this);

        MessageBroker.Default.Receive<PlayerTurnSignal>().Subscribe(_ =>
        {
            if (_.GameObj == gameObject)
            {
                var regenPts = 45;
                if (_CurrActionPts.Value >= _CharData.ActionPoints)
                {
                    return;
                    regenPts = 0;
                }

                _ActiveSkill = SkillType.None;
                _ActiveSkillable = null;

                MessageBroker.Default.Publish(new CallRegenSignal { Damage = regenPts, Lifetime = 2, Position = transform.position });

                _CurrActionPts.Value += regenPts;
                _CurrActionPts.Value = Mathf.Clamp(_CurrActionPts.Value, 0, _CharData.ActionPoints);



            }
        }).AddTo(this); 
    }

    private void Retreat()
    {
        _ActionState = ActionState.Retreat;
        var clickBuildingInfoStream = Observable.EveryUpdate();

        var subscription = new CompositeDisposable();

        bool endAttack = false;

        clickBuildingInfoStream.Subscribe(x =>
        {
            if (subscription.IsDisposed)
                return;

            if (_Animator.GetCurrentAnimatorStateInfo(0).IsName(AnimConstants.ATTACK_ANIM))
            {
                endAttack = true;
            }

            if (endAttack)
            {
                if (_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= .5f)
                {
                    transform.position = Vector2.MoveTowards(transform.position, _OriginalPosition, _CharData.MoveSpeed);

                    _Animator.Play(AnimConstants.RETREAT_ANIM);

                    var distance = Vector2.Distance(transform.position, _OriginalPosition);

                    if (distance < .1f)
                    {
                        _Animator.Play(AnimConstants.IDLE_ANIM);

                        //MessageBroker.Default.Publish(new EndActionSignal());
                        subscription.Dispose();
                    }
                }
            }
        }).AddTo(subscription);
    }

    private void AttackEnemy()
    {
        _ActionState = ActionState.Engage;
        var clickBuildingInfoStream = Observable.EveryUpdate();


        var subscription = new CompositeDisposable();
        clickBuildingInfoStream.Subscribe(x =>
        {
            if (subscription.IsDisposed)
                return;

            if (_ActiveSkillable == null)
                _ActiveSkillable = _Skills.Find(_ => _.SkillData.SkillType == SkillType.NormalSlash).ISkillable();

            var charCalculatedDmg = _CharData.Damage * _AccuracyMagnitude;
            Debug.LogError("Char damage is : " + charCalculatedDmg);
            if (_TargetObjects.Count > 0)
            _ActiveSkillable.TriggerSkill(_TargetObjects, charCalculatedDmg);


            _Animator.Play(AnimConstants.ATTACK_ANIM);
            _ActionState = ActionState.Attack;
            Retreat();
            subscription.Dispose();
        }).AddTo(subscription);
    }

    public UnitType GetUnitType()
    {
        return _UnitType;
    }

    public void TakeDamage(float dmg)
    {
        _CurrentHealth.Value -= dmg;
        _Animator.Play(AnimConstants.HIT_ANIM);

        _CurrentHealth.Value = Mathf.Clamp(_CurrentHealth.Value, 0, 100);
        if (_CurrentHealth.Value  <= 0)
        {
            MessageBroker.Default.Publish(new UnitDiedSignal { UnittType = UnitType.Player, GameObject = gameObject });
            _ActionState = ActionState.Die;
            _Animator.Play(AnimConstants.DIE_ANIM);
        }
    }

    void SelfTarget(SkillType buffType)
    {
        var currSkill = _Skills.Find(_ => _.SkillData.SkillType == buffType);
        var apCost = currSkill.SkillData.APCost;

        currSkill.ISkillable().TriggerSkill(new TargetData { Object = gameObject }, _CharData.Damage);

     
    }

    void UseOffensiveSkill(SkillType offenseType)
    {
        var currSkill = _Skills.Find(_ => _.SkillData.SkillType == offenseType);
        _ActiveSkillable = currSkill.ISkillable();
    }

    public void TakeDamage(float dmg, Vector3 dmgPoint)
    {
        throw new System.NotImplementedException();
    }

    public void Heal(float dmg)
    {
        _CurrentHealth.Value += dmg;
        _CurrentHealth.Value = Mathf.Clamp(_CurrentHealth.Value, 0, _CharData.Health);
        _Animator.Play(AnimConstants.CAST_ANIM);

    }
}