﻿using UnityEngine;

public class GenericEnemy : MonoBehaviour
{
    [SerializeField]
    protected UnitType _UnitType;

    [SerializeField]
    protected float _CurrentHealth;

    [SerializeField]
    protected ActionState _ActionState;

    [SerializeField]
    protected Vector3 _OriginalPosition;

    [SerializeField]
    protected GameObject _TargetObj;

    [SerializeField]
    protected MonsterData _CharData;

    [SerializeField]
    protected Sprite[] _SpriteArray;

    [SerializeField]
    protected Animator _Animator;

    public virtual void Awake()
    {
        _CurrentHealth = _CharData.Health;
    }
}