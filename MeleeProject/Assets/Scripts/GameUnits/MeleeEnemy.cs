﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class MeleeEnemy : GenericEnemy, IDamageable, ITurnable, IAICommand
{
    [SerializeField]
    private Image _HealthImage;

    [SerializeField]
    private Transform _Head, _Body;

    [SerializeField]
    private BoxCollider2D _Collider;

    public override void Awake()
    {
        base.Awake();
        _OriginalPosition = transform.position;
    }

    private void Retreat()
    {
        _ActionState = ActionState.Retreat;
        var clickBuildingInfoStream = Observable.EveryUpdate();

        var subscription = new CompositeDisposable();

        bool endAttack = false;

        clickBuildingInfoStream.Subscribe(x =>
        {
            if (subscription.IsDisposed)
                return;

            if (_Animator.GetCurrentAnimatorStateInfo(0).IsName(AnimConstants.ATTACK_ANIM))
            {
                endAttack = true;
            }

            if (endAttack)
            {
                if (_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= .5f)
                {
                    transform.position = Vector2.MoveTowards(transform.position, _OriginalPosition, _CharData.MoveSpeed);

                    _Animator.Play(AnimConstants.RETREAT_ANIM);

                    var distance = Vector2.Distance(transform.position, _OriginalPosition);

                    if (distance < .1f)
                    {
                        MessageBroker.Default.Publish(new EndActionSignal());
                        _Animator.Play(AnimConstants.IDLE_ANIM);
                        subscription.Dispose();
                    }
                }
            }
        }).AddTo(subscription);
    }

    private void AttackEnemy()
    {
        _ActionState = ActionState.Engage;
        var clickBuildingInfoStream = Observable.EveryUpdate();

        var subscription = new CompositeDisposable();
        clickBuildingInfoStream.Subscribe(x =>
        {
            if (subscription.IsDisposed)
                return;

            //DISABLED ENGAGING FEATURE
            /*
            transform.position = Vector2.MoveTowards(transform.position, _TargetObj.transform.position, _CharData.MoveSpeed);

            _Animator.Play(AnimConstants.RUN_ANIM);

            var distance = Vector2.Distance(transform.position, _TargetObj.transform.position);

            if (distance < _CharData.Range)
            {
                MessageBroker.Default.Publish(new CallDamageSignal { Damage = _CharData.Damage, Lifetime = .5f, Position = _TargetObj.transform.position });
                MessageBroker.Default.Publish(new CallVFXSignal { Position = _TargetObj.transform.position, Type = VFXType.SwordHit, LifeSpan = .5f });
                _TargetObj.GetComponent<IDamageable>().TakeDamage(_CharData.Damage);
                _Animator.Play(AnimConstants.ATTACK_ANIM);
                _ActionState = ActionState.Attack;
                Retreat();
                subscription.Dispose();
            }*/


            MessageBroker.Default.Publish(new CallDamageSignal { Damage = _CharData.Damage, Lifetime = .5f, Position = _TargetObj.transform.position });
            MessageBroker.Default.Publish(new CallVFXSignal { Position = _TargetObj.transform.position, Type = VFXType.SwordHit, LifeSpan = .5f });
            _TargetObj.GetComponent<IDamageable>().TakeDamage(_CharData.Damage);
            _Animator.Play(AnimConstants.ATTACK_ANIM);
            _ActionState = ActionState.Attack;
            Retreat();
            subscription.Dispose();
        }).AddTo(subscription);
    }

    public UnitType GetUnitType()
    {
        return _UnitType;
    }

    public void SetState(ActionState state)
    {
        if (_ActionState != ActionState.Die)
        {
            if (state == ActionState.Attack)
            {
                AttackEnemy();
            }
        }
    }

    public void TakeDamage(float dmg)
    {
        _Animator.Play(AnimConstants.HIT_ANIM);
        _CurrentHealth -= dmg;
        _CurrentHealth = Mathf.Clamp(_CurrentHealth, 0, 100);
        _HealthImage.fillAmount = _CurrentHealth / _CharData.Health;

        if (_CurrentHealth <= 0)
        {
            MessageBroker.Default.Publish(new UnitDiedSignal { UnittType = UnitType.Enemy, GameObject = gameObject });
            _ActionState = ActionState.Die;
            _Animator.Play(AnimConstants.DIE_ANIM);

            _Collider.enabled = false;
        }
    }

    public void TakeDamage(float dmg, Vector3 dmgPoint)
    {
        var headDistance = Vector2.Distance(_Head.position, dmgPoint);
        var bodyDistance = Vector2.Distance(_Body.position, dmgPoint);



        float alteredDmg = 0;
        if (headDistance < bodyDistance)
        {
            alteredDmg = dmg - (headDistance * dmg);
        }
        else
        {
            alteredDmg = dmg - (bodyDistance * dmg);
            alteredDmg -= alteredDmg / 4;

            float randomizer = Random.Range(0, 5);
            if(randomizer > 3)
            {
                MessageBroker.Default.Publish(new CallVFXSignal { Type = VFXType.BlockDMG, LifeSpan = .5f, Position = transform.position });
                return;
            }
        }

        Debug.LogError(gameObject.name + " :: " + alteredDmg);

        MessageBroker.Default.Publish(new CallVFXSignal { Position = dmgPoint, Type = VFXType.SwordHit, LifeSpan = .5f });
        MessageBroker.Default.Publish(new CallDamageSignal { Damage = alteredDmg, Lifetime = .5f, Position = transform.position });

        TakeDamage(alteredDmg);
    }

    public void Heal(float dmg)
    {
        _CurrentHealth += dmg;
        _CurrentHealth = Mathf.Clamp(_CurrentHealth, 0, _CharData.Health);
    }
}