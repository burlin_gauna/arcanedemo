﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathClass
{
    public static Vector3 GetMidPosition(List<TargetData> coords)
    {
        Vector3 newVec = new Vector3(0, 0, 0);
        for (int i = 0; i < coords.Count; i++)
        {
            newVec += coords[i].Object.transform.position;
        }
        return newVec / coords.Count;
    }

}
 