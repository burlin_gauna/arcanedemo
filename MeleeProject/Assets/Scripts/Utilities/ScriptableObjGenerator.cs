﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ScriptableObjGenerator : MonoBehaviour
{
    private static void Process(Object asset)
    {
        AssetDatabase.CreateAsset(asset, "Assets/"+asset.GetType().ToString()+".asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
    
    [MenuItem("Assets/Create/Create CharacterData Object")]
    public static void Create_CharacterData()
    {
        CharacterData asset = ScriptableObject.CreateInstance<CharacterData>();
        Process(asset);
    }
    [MenuItem("Assets/Create/Create MonsterData Object")]
    public static void Create_MonsterData()
    {
        MonsterData asset = ScriptableObject.CreateInstance<MonsterData>();
        Process(asset);
    }
    [MenuItem("Assets/Create/Create SkillData Object")]
    public static void Create_SkillData()
    {
        SkillData asset = ScriptableObject.CreateInstance<SkillData>();
        Process(asset);
    }
    [MenuItem("Assets/Create/Create ItemData Object")]
    public static void Create_ItemData()
    {
        ItemData asset = ScriptableObject.CreateInstance<ItemData>();
        Process(asset);
    }
    [MenuItem("Assets/Create/Create InventoryDatabase Object")]
    public static void Create_InventoryDatabase()
    {
        InventoryDatabase asset = ScriptableObject.CreateInstance<InventoryDatabase>();
        Process(asset);
    }
    
}
