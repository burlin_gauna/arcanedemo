﻿public static class AnimConstants
{
    public static string ATTACK_ANIM = "Attack";
    public static string DIE_ANIM = "Die";
    public static string RUN_ANIM = "Run";
    public static string HIT_ANIM = "Hit";
    public static string IDLE_ANIM = "Idle";
    public static string RETREAT_ANIM = "Retreat";
    public static string ACTIVATE_ANIM = "Activate";
    public static string DEACTIVATE_ANIM = "Deactivate";
    public static string CAST_ANIM = "Cast";
}