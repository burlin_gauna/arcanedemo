﻿using System.Collections.Generic;
using UnityEngine;

public interface ISkillable
{
    void TriggerSkill(List<TargetData> targets, float rawValue);
    void TriggerSkill(TargetData targets, float rawValue);
}