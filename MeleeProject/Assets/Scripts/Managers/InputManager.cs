﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    private EnemyManager _EnemyManager;
    private GameManager _GameManager;

    [SerializeField]
    private GameObject _AimingUI;

    [SerializeField]
    private GenericCharacter _CurrentCharacter;
    public void SetCharacterReference(GenericCharacter currChar)
    {
        _CurrentCharacter = currChar;
    }
    [SerializeField]
    private GameObject _AttackIndicator;

    [SerializeField]
    private Transform _RangeGauge, _Pivot;

    [SerializeField]
    private Image _RangeFill,_MarkerFill, _RandomizedFill;

    [SerializeField]
    private Animator _Animator;

    [SerializeField]
    private bool _PlayerTurn, _TriggeredAttack, _SkillSelected;

    [SerializeField]
    IDisposable _EnemyActionDisp;

    [SerializeField]
    private float _MagnitudeMeter;
    bool _FlipMagnitudeMeter;

    List<TargetData> _CurrentTargets = new List<TargetData>();

    [SerializeField]
    private List<GameObject> _PlayerIndicators;

    [SerializeField]
    private List<SkillButtonTemplate> _SkillButtons;
    [SerializeField]
    private SkillButtonTemplate _CurrentSkillTemp;

    [SerializeField]
    private Button _EndTurnButton;

    [SerializeField]
    private Animator _SkillTabAnim, _AimingAnim;

    [SerializeField]
    private float _RandomizedMagnitude;

    float magnitudeSpeed = 1;

    public void TriggerPlayerTurn()
    {
        _MagnitudeMeter = 0;
        _RangeFill.fillAmount = 0;
        _PlayerTurn = true;
        _SkillSelected = false;
        _TriggeredAttack = false;
        _SkillTabAnim.Play(AnimConstants.ACTIVATE_ANIM);
        for (int i = 0; i < _PlayerIndicators.Count; i++)
        {
            _PlayerIndicators[i].SetActive(true);
        }

        _RandomizedMagnitude = UnityEngine.Random.Range(.3f, 1);
        _RandomizedFill.fillAmount = _RandomizedMagnitude;
        _MarkerFill.fillAmount = 1 -(_RandomizedMagnitude - .025f);
    }

    private void Awake()
    {
        Factory.Register<InputManager>(this);

        int buttonCount = _SkillButtons.Count;

        for (int i = 0; i < buttonCount;i++ )
        {
            var data = _SkillButtons[i];
            _SkillButtons[i].Button.onClick.AddListener(() => 
            {
                TriggerSkillSignal(data);
            });
        }

        _EndTurnButton.onClick.AddListener(() => 
        {
            MessageBroker.Default.Publish(new EndActionSignal());
            _SkillTabAnim.Play(AnimConstants.DEACTIVATE_ANIM);
            _AimingUI.SetActive(false);
            for (int i = 0; i < _PlayerIndicators.Count; i++)
            {
                _PlayerIndicators[i].SetActive(false);
            }
        });
    }

    private void TriggerSkillSignal(SkillButtonTemplate skillTemplate)
    {
        if (_PlayerTurn)
        {
            if(_CurrentCharacter.CurrentAP >= skillTemplate.SkillData.APCost)
            {
                MessageBroker.Default.Publish(new CallDamageSignal
                {
                    Damage = skillTemplate.SkillData.APCost,
                    Position = _CurrentCharacter.transform.position,
                    Lifetime = .5f
                });
                _CurrentCharacter.DeductAP(_CurrentSkillTemp.SkillData.APCost);
            }
            else
            {
                MessageBroker.Default.Publish(new CallVFXSignal
                {
                    Type = VFXType.NoMana,
                    Position = _CurrentCharacter.transform.position,
                    LifeSpan = .5f
                });
                return;
            }


            if (skillTemplate.SkillData.TargetType != TargetType.Self)
            {
                _SkillSelected = true;
                _SkillTabAnim.Play(AnimConstants.DEACTIVATE_ANIM);
            }
            _CurrentSkillTemp.Icon.sprite = skillTemplate.Icon.sprite;
            MessageBroker.Default.Publish(new TriggerSkillSignal
            {
                SkillData = skillTemplate.SkillData
            });

        }
    }

    private void Start()
    {
        _GameManager = Factory.Get<GameManager>();
        _EnemyManager = Factory.Get<EnemyManager>();

        _EnemyManager.RequestEnemy(true);
        _Animator.Play(AnimConstants.ACTIVATE_ANIM);
    }

    public void PreviousTarget()
    {
        var enemy = _EnemyManager.RequestEnemy(false);
        MessageBroker.Default.Publish(new EnemyTargetSignal { EnemyObject = enemy });
        _AttackIndicator.transform.position = enemy.transform.position;
        _Animator.Play(AnimConstants.ACTIVATE_ANIM);
    }

    public void NextTarget()
    {
        var enemy = _EnemyManager.RequestEnemy(true);
        MessageBroker.Default.Publish(new EnemyTargetSignal { EnemyObject = enemy });
        _AttackIndicator.transform.position = enemy.transform.position;
        _Animator.Play(AnimConstants.ACTIVATE_ANIM);
    }

    public void Attack()
    {
        _Animator.Play(AnimConstants.DEACTIVATE_ANIM);
        _AttackIndicator.SetActive(false);

        var calculatedMagnitude =  1- (Mathf.Abs(_RandomizedMagnitude - _MagnitudeMeter) / _RandomizedMagnitude);
        Debug.LogError("Magnitude is : " + calculatedMagnitude * 100);

        MessageBroker.Default.Publish(new AttackSignal
        {
            EnemyList = _CurrentTargets,
            Magnitude = calculatedMagnitude,
            PlayerObj = _CurrentCharacter.gameObject
        });
    }

    private void Update()
    {
        if (_SkillSelected && _GameManager.CurrentEntityTurn == UnitType.Player && _PlayerTurn)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                PreviousTarget();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                NextTarget();
            }
            if(Input.GetKey(KeyCode.Mouse0))
            {
                if(!_TriggeredAttack)
                    _TriggeredAttack = true;
                if(_TriggeredAttack)
                {
                    _AimingUI.SetActive(true);
                }
                Vector3 dir = Input.mousePosition - Camera.main.WorldToScreenPoint(_Pivot.position);
                float angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;

                angle = Mathf.Clamp(angle, -120, -40);
                _Pivot.rotation = Quaternion.AngleAxis(-angle, Vector3.forward);


                _CurrentTargets = new List<TargetData>();
                Debug.DrawRay(_Pivot.transform.position, _Pivot.transform.up * 10, Color.red);
                RaycastHit2D[] hit = Physics2D.RaycastAll(_Pivot.position, _Pivot.up * 100);
                for (int i = 0; i < hit.Length; i++)
                {
                    if (hit[i].collider != null)
                    {
                        if (hit[i].collider.GetComponent<IDamageable>() != null)
                            _CurrentTargets.Add(new TargetData { Object = hit[i].collider.gameObject, Coordinates = hit[i].point });

                    }
                }

                if (_FlipMagnitudeMeter == false)
                {
                    _MagnitudeMeter += Time.deltaTime * magnitudeSpeed;
                    if (_MagnitudeMeter >= 1)
                    {
                        _FlipMagnitudeMeter = true;
                    }
                }
                else
                {
                    _MagnitudeMeter -= Time.deltaTime * magnitudeSpeed;
                    if (_MagnitudeMeter <= 0)
                    {
                        _FlipMagnitudeMeter = false;
                    }
                }


                _MagnitudeMeter = Mathf.Clamp01(_MagnitudeMeter);

                _RangeFill.fillAmount = _MagnitudeMeter;

            }
            else
            {
                if (!_TriggeredAttack)
                    return;
                _TriggeredAttack = true;
                _PlayerTurn = false;

                _AimingAnim.Play(AnimConstants.ACTIVATE_ANIM);
                _EnemyActionDisp = Observable.Interval(TimeSpan.FromMilliseconds(500)).Subscribe(x =>
                {
                    _AimingUI.SetActive(false);
                    Attack();
                    _EnemyActionDisp.Dispose();
                });
                return;
            }


        }
    }
}