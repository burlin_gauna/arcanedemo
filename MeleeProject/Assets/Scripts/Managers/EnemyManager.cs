﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField]
    private List<GenericEnemy> _EnemyList;

    private int index;
    private int listCount;

    private void Awake()
    {
        Factory.Register<EnemyManager>(this);
    }

    private void Start()
    {
        listCount = _EnemyList.Count;
    }

    public GameObject RequestEnemy(bool ifPositive)
    {
        if (ifPositive)
        {
            index++;
            if (index >= listCount)
                index = 0;
        }
        else
        {
            index--;
            if (index < 0)
                index = listCount - 1;
        }
        return _EnemyList[index].gameObject;
    }
}