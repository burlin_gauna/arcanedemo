﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class VFXManager : MonoBehaviour
{


    [SerializeField]
    private List<VFXEntity> _VFXList , _VFXSpawned;


    [SerializeField]
    private GameObject _RegenIndicator;

    [SerializeField]
    private Animator _RegenAnimator;

    [SerializeField]
    private Text  _RegenText;


    private void Awake()
    {
        MessageBroker.Default.Receive<CallVFXSignal>().Subscribe(_ =>
        {
            RequestVFX(_.Position, _.Type, _.LifeSpan);
        }).AddTo(this);

        MessageBroker.Default.Receive<CallDamageSignal>().Subscribe(_ =>
        {
            DamageIndicator(_.Position, _.Lifetime, _.Damage);
        }).AddTo(this);

        MessageBroker.Default.Receive<CallRegenSignal>().Subscribe(_ =>
        {
            RegenIndicator(_.Position, _.Lifetime, _.Damage);
        }).AddTo(this);

        MessageBroker.Default.Receive<DisposeVFXSignal>().Subscribe(_ =>
        {
            _VFXList.Add(_.Entity);
            _VFXSpawned.Remove(_.Entity);
        }).AddTo(this);
    }

    private void RequestVFX(Vector3 position, VFXType vfxType, float life)
    {

        var vfx = _VFXList.Find(_ => _.VFXType == vfxType);
        if (vfx == null)
            return;
        _VFXSpawned.Add(vfx);
        _VFXList.Remove(vfx);

        vfx.SetLifespan(life);
        vfx.transform.position = position;
        vfx.gameObject.SetActive(true);
    }

    private void DamageIndicator(Vector3 position, float life, float damage)
    {
        var vfx = _VFXList.Find(_ => _.VFXType == VFXType.DamageNumber);
        _VFXSpawned.Add(vfx);
        _VFXList.Remove(vfx);

        vfx.transform.position = position;
        vfx.SetLifespan(life);
        vfx.gameObject.SetActive(true);

        vfx.GetComponent<VFXNumber>().LaunchTextAnim(damage.ToString("f0"));
    }

    private void RegenIndicator(Vector3 position, float life, float damage)
    {
        _RegenIndicator.transform.position = position;
        _RegenIndicator.GetComponent<VFXEntity>().SetLifespan(life);
        _RegenText.text = damage.ToString();
        _RegenIndicator.SetActive(true);
        _RegenAnimator.Play(AnimConstants.ACTIVATE_ANIM);
    }
}