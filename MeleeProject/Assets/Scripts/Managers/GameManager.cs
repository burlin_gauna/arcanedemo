﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private InputManager _InputManager;
    private EnemyManager _EnemyManager;

    IDisposable _EnemyActionDisp, _AllyActionDisp;

    [SerializeField]
    private UnitType _CurrentEntityTurn;

    public UnitType CurrentEntityTurn { get { return _CurrentEntityTurn; } }

    [SerializeField]
    private List<GameObject> _TurnEntities;

    private int indexTurn;
    private int maxIndex;

    private int playerCount;
    private void Awake()
    {
        Factory.Register<GameManager>(this);

        MessageBroker.Default.Receive<UnitDiedSignal>().Subscribe(_ =>
        {
            UnitDied(_);
        }).AddTo(this);

        MessageBroker.Default.Receive<EndActionSignal>().Subscribe(_ =>
        {
            EndTurn();
        }).AddTo(this);
    }

    private void Start()
    {
        _InputManager = Factory.Get<InputManager>();
        _EnemyManager = Factory.Get<EnemyManager>();

        foreach (var temp in _TurnEntities)
        {
            if (temp.GetComponent<ITurnable>().GetUnitType() == UnitType.Player)
            {
                playerCount++;
            }
        }


        maxIndex = _TurnEntities.Count;
        indexTurn = 0;

        EndTurn();

        /*_CurrentEntityTurn = _TurnEntities[indexTurn].GetComponent<ITurnable>().GetUnitType();

        _InputManager.SetCharacterReference(_TurnEntities[indexTurn].GetComponent<GenericCharacter>());

        _InputManager.TriggerPlayerTurn();*/
    }

    private void UnitDied(UnitDiedSignal _)
    {
        var entity = _TurnEntities.Find(q => q == _.GameObject);

        if (entity.GetComponent<ITurnable>().GetUnitType() == UnitType.Player)
            playerCount--;

        _TurnEntities.Remove(entity);
        maxIndex = _TurnEntities.Count;
        _InputManager.NextTarget();
    }

    private void EndTurn()
    {
        if (maxIndex <= 1 || playerCount <= 0)
        {
            Debug.LogError("Game has Ended");
            return;
        }

        indexTurn++;
        if (indexTurn >= maxIndex)
        {
            Debug.LogError("Turn has Ended");
            MessageBroker.Default.Publish(new EndRoundSignal());
            indexTurn = 0;
        }

        _CurrentEntityTurn = _TurnEntities[indexTurn].GetComponent<ITurnable>().GetUnitType();
        if (_CurrentEntityTurn == UnitType.Enemy)
        {
            _EnemyActionDisp =  Observable.Interval(TimeSpan.FromMilliseconds(500)).Subscribe(x =>
            {
                //Debug.LogError(_TurnEntities[indexTurn].gameObject.name + " Attacking");
                _TurnEntities[indexTurn].GetComponent<IAICommand>().SetState(ActionState.Attack);
                _EnemyActionDisp.Dispose();
            });

        }
        else if (_CurrentEntityTurn == UnitType.Player)
        {
            var subscription = new CompositeDisposable();
            var clickBuildingInfoStream = Observable.Interval(TimeSpan.FromMilliseconds(1000)).Subscribe(x =>
            {
                MessageBroker.Default.Publish(new PlayerTurnSignal { GameObj = _TurnEntities[indexTurn].gameObject });
                _InputManager.TriggerPlayerTurn();
                _InputManager.SetCharacterReference(_TurnEntities[indexTurn].GetComponent<GenericCharacter>());
                subscription.Dispose();
            }).AddTo(subscription);
        }
        else if (_CurrentEntityTurn == UnitType.Ally)
        {
            _InputManager.SetCharacterReference(_TurnEntities[indexTurn].GetComponent<GenericCharacter>());

            List<TargetData> newTargets = new List<TargetData>();
            newTargets.Add( new TargetData { Object = _EnemyManager.RequestEnemy(true) });

            _AllyActionDisp = Observable.Interval(TimeSpan.FromMilliseconds(500)).Subscribe(x =>
            {
                MessageBroker.Default.Publish(new AttackSignal
                {
                    EnemyList = newTargets,
                    Magnitude = .8f,
                    PlayerObj = _TurnEntities[indexTurn].gameObject
                });
                MessageBroker.Default.Publish(new EndActionSignal());
                _AllyActionDisp.Dispose();
            });
        }
    }
}