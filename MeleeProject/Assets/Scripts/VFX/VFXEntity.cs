﻿using System;
using UniRx;
using UnityEngine;

public class VFXEntity : MonoBehaviour
{
    private IDisposable _update;
    public VFXType VFXType;
    public void SetLifespan(float life)
    {
        _update = Observable.Interval(TimeSpan.FromMilliseconds(life * 1000)).Subscribe(x =>
        {
            _update.Dispose();
            MessageBroker.Default.Publish(new DisposeVFXSignal { Entity = this });
            gameObject.SetActive(false);
        });
    }
}