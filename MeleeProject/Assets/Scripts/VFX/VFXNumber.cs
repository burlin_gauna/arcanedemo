﻿using UnityEngine;
using UnityEngine.UI;

public class VFXNumber : MonoBehaviour
{
    [SerializeField]
    private Animator _Anim;

    [SerializeField]
    private Text _Text;

    public void LaunchTextAnim(string text)
    {
        _Text.text = text;
        _Anim.Play(AnimConstants.ACTIVATE_ANIM);
    }
}