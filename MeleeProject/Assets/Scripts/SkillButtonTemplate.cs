﻿using UnityEngine;
using UnityEngine.UI;

public class SkillButtonTemplate : MonoBehaviour
{
    public Image Icon;
    public Button Button;
    public SkillData SkillData;
}