﻿using UnityEngine;

public class UnitDiedSignal
{
    public UnitType UnittType;
    public GameObject GameObject;
}