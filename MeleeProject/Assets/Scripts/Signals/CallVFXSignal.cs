﻿using UnityEngine;

public class CallVFXSignal
{
    public Vector3 Position;
    public VFXType Type;
    public float LifeSpan;
}