﻿using System.Collections.Generic;
using UnityEngine;

public class AttackSignal
{
    public GameObject PlayerObj;
    public List<TargetData> EnemyList;
    public float Magnitude;
}