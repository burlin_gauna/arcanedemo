﻿using UnityEngine;

public class CallDamageSignal
{
    public float Damage;
    public Vector3 Position;
    public float Lifetime;
}