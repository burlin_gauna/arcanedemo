﻿using UnityEngine;

public class CallRegenSignal
{
    public float Damage;
    public Vector3 Position;
    public float Lifetime;
}