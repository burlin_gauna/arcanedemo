﻿public enum TargetType
{
    SingleEnemy,
    MultipleEnemy,
    SingleAlly,
    Self,
    MultipleAlly
}