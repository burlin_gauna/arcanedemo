﻿public enum UnitType
{
    Player,
    Enemy,
    Neutral,
    Ally
}