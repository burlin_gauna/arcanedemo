﻿public enum ActionState
{
    Idle,
    Engage,
    Attack,
    Retreat,
    Die
}