﻿using UnityEngine;

public class GenericSkill : MonoBehaviour
{
    [SerializeField]
    protected SkillData _SkillData;

    public SkillData SkillData { get { return _SkillData; } }

    private ISkillable _Skillable;

    public ISkillable ISkillable()
    {
        if (_Skillable == null)
        {
            _Skillable = GetComponent<ISkillable>();
        }
        return _Skillable;
    }
}