﻿using UnityEngine;

public class CharacterData : ScriptableObject
{
    public float Health;
    public float Armor;
    public float Damage;
    public float Range;
    public float MoveSpeed;
    public float ActionPoints;
}
public class TargetData
{
    public GameObject Object;
    public Vector3 Coordinates;
}