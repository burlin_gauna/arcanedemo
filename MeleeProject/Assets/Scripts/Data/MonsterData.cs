﻿using UnityEngine;

public class MonsterData : ScriptableObject
{
    public float Health;
    public float Armor;
    public float Damage;
    public float Range;
    public float MoveSpeed;
}