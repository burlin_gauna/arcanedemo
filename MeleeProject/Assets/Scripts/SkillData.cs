﻿using System.Collections.Generic;
using UnityEngine;

public class SkillData : ScriptableObject
{
    public float APCost;
    public SkillType SkillType;
    public TargetType TargetType;
}