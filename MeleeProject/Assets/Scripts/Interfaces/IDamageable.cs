﻿using UnityEngine;

public interface IDamageable
{
    void TakeDamage(float dmg);
    void Heal(float dmg);
    void TakeDamage(float dmg, Vector3 dmgPoint);
}