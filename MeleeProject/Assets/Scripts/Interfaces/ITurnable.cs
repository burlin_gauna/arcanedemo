﻿public interface ITurnable
{
    UnitType GetUnitType();
}