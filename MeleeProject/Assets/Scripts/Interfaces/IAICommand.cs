﻿public interface IAICommand
{
    void SetState(ActionState state);
}