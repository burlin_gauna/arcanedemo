﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class InventoryTemplate2nd : MonoBehaviour
{
    public void ClickButton()
    {
        MessageBroker.Default.Publish(new ClickItem { Data = Data });
    }
    public ItemData Data;
    public Text Description;
    public Image Icon;
    public void SetData(ItemData data)
    {
        Data = data;
        Description.text = data.Description;
        Icon.sprite = data.Sprite;
    }
}
