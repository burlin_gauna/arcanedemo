﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData : ScriptableObject
{
    public string ItemName;
    public string Description;
    public ItemType ItemType;
    public Sprite Sprite;
    public List<ItemData> CraftRecipe;

    public bool MatchRecipe(List<ItemData> dataList)
    {
        int compareCount = dataList.Count;
        int recipeCount = CraftRecipe.Count;

        if (recipeCount < compareCount)
            return false;

        int comapreResult = 0;
        for (int i = 0; i < recipeCount; i++)
        {
            if(dataList.Contains(CraftRecipe[i]))
            {
                comapreResult++;
            }
        }
        if (comapreResult == compareCount)
            return true;

        return false;
    }
}


public enum ItemType
{
    Weapon,
    Armor,
    Material,
    Consumable
}
public class ClickItem
{
    public ItemData Data;
}
[Serializable]
public class InventoryCombo
{
    public ItemData Item;
    public int Quantity;
}
