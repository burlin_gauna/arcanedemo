﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryDatabase : ScriptableObject
{
    public List<ItemData> ItemDataList;
}
