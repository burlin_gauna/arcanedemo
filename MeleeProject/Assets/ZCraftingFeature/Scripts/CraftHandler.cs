﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class CraftHandler : MonoBehaviour
{
    [SerializeField]
    private List<InventoryTemplate> _CurrentInventory;

    [SerializeField]
    private InventoryTemplate _CraftSlot1, _CraftSlot2;

    [SerializeField]
    private InventoryTemplate __CraftResultSlot;

    [SerializeField]
    private ItemData _CurrentItemData;
    [SerializeField]
    private Image _CurrentItemImage;

    [SerializeField]
    private List<InventoryCombo> _CurrentItems;

    [SerializeField]
    private bool _DragItem;

    [SerializeField]
    private int _CurrentHoverSlot;


    [SerializeField]
    private Canvas _CurrCanvas;

    [SerializeField]
    private ItemData _CraftSlotData1, _CraftSlotData2;

    [SerializeField]
    private InventoryDatabase _InventoryDatabase;

    private void Awake()
    {
        MessageBroker.Default.Receive<ClickItem>().Subscribe(_ =>
        {
            if (_DragItem)
                return;

            _CurrentItemData = _.Data;
            _CurrentItemImage.sprite = _.Data.Sprite;
            _DragItem = true;
            _CurrentItemImage.enabled = true;
        }).AddTo(this);

        for(int i = 0; i < _CurrentItems.Count; i++)
        {
            _CurrentInventory[i].Init(_CurrentItems[i].Item);
        }
    }

    private void Update()
    {
        if(_DragItem)
        {
            Vector2 pos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_CurrCanvas.transform as RectTransform, Input.mousePosition, _CurrCanvas.worldCamera, out pos);
            _CurrentItemImage.transform.position = _CurrCanvas.transform.TransformPoint(pos);



            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
               
                _DragItem = false;
                int newSlot = _CurrentHoverSlot;
                StartCoroutine(DelayClick(newSlot));
            }
        }
    }

    IEnumerator DelayClick(int slot)
    {
        yield return new WaitForSeconds(.15f);

        switch (slot)
        {
            case 1:
                _CraftSlotData1 = _CurrentItemData;
                _CraftSlot1.Init(_CurrentItemData);
                break;
            case 2:
                _CraftSlotData2 = _CurrentItemData;
                _CraftSlot2.Init(_CurrentItemData);
                break;
        }
        CombineItems();

        _CurrentItemData = null;
        _CurrentItemImage.enabled = false;
    }

    private void CombineItems()
    {
        if (!(_CraftSlotData1 != null && _CraftSlotData2 != null))
        {
            Debug.LogError("Incomplete Recipe");
            __CraftResultSlot.EmptySlot();
            return;
        }



        var masterList = _InventoryDatabase.ItemDataList;
        List<ItemData> compileList = new List<ItemData>();
        compileList.Add(_CraftSlotData1);
        compileList.Add(_CraftSlotData2);


        for (int i = 0; i < masterList.Count; i++)
        {
            var ifMatch = masterList[i].MatchRecipe(compileList);
            Debug.LogError("Result bool : " + i + " : " + masterList[i].MatchRecipe(compileList));
            if (ifMatch)
            {
                Debug.LogError("Result  : " + i + " : " + masterList[i].ItemName);
                __CraftResultSlot.Init(masterList[i]);
            }
        }
    }

    public void ClickCraftSlot(int i)
    {
        switch (i)
        {
            case 1:
                _CraftSlot1.EmptySlot();
                _CraftSlotData1 = null;
                break;
            case 2:
                _CraftSlot2.EmptySlot();
                _CraftSlotData2 = null;
                break;
        }
        CombineItems();
    }

    public void HoverCraftSlot(int i)
    {
        _CurrentHoverSlot = i;

    }
    public void UnHoverCraftSlot(int i)
    {
        _CurrentHoverSlot = 0;
    }
}
