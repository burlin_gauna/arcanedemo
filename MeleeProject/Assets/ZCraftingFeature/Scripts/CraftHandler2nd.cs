﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class CraftHandler2nd : MonoBehaviour
{
    [SerializeField]
    private List< InventoryTemplate> _CraftSlot;

    [SerializeField]
    private InventoryTemplate _CraftResultSlot;

    [SerializeField]
    private List<InventoryTemplate> _InventoryTemplates;

    [SerializeField]
    private List<InventoryCombo> _CurrentItems;


    [SerializeField]
    private List<InventoryTemplate2nd> _CraftOptions;

    [SerializeField]
    private ItemData _CurrentItemData;

    [SerializeField]
    private Text _Description;



    [SerializeField]
    private InventoryDatabase _InventoryDatabase;

    private void Awake()
    {
        MessageBroker.Default.Receive<ClickItem>().Subscribe(_ =>
        {
            _CurrentItemData = _.Data;
            _CraftResultSlot.Init(_.Data);
            _Description.text = _.Data.Description;
            DisplayRequirements();
        }).AddTo(this);

        var masterData = _InventoryDatabase.ItemDataList;
        for (int i = 0; i < masterData.Count; i++)
        {
            _CraftOptions[i].SetData(masterData[i]);
        }
        for (int i = 0; i < _CurrentItems.Count; i++)
        {
            _InventoryTemplates[i].Init(_CurrentItems[i].Item);
        }
        
    }

    public void CancelCraft()
    {
        _CraftResultSlot.EmptySlot();
        _Description.text = "";
        for (int i = 0; i < _CraftSlot.Count; i++)
        {
            _CraftSlot[i].EmptySlot();
        }
    }

    void DisplayRequirements()
    {
        List<ItemData> dataList = _CurrentItemData.CraftRecipe;
        for(int i = 0; i < _CraftSlot.Count; i++)
        {
            _CraftSlot[i].EmptySlot();
        }
        for (int i = 0; i < dataList.Count; i++)
        {
            _CraftSlot[i].Init(dataList[i]);
        }
    }
}
