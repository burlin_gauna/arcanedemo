﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class InventoryTemplate : MonoBehaviour
{
    public Image ImageIcon;
    public ItemData ItemData;
    public bool IsActive;

    public void OnClickButton()
    {
        if(IsActive == true)
        MessageBroker.Default.Publish(new ClickItem { Data = ItemData });
    }

    public void EmptySlot()
    {
        IsActive = false;
        ItemData = null;
        ImageIcon.sprite = null;
        ImageIcon.enabled = false;
    }


    public void Init(ItemData data)
    {
        IsActive = true;
        ItemData = data;
        ImageIcon.enabled = true;
        ImageIcon.sprite = data.Sprite;
    }
}
