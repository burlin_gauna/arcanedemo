﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class GreaterSlash : GenericSkill, ISkillable
{
    public void TriggerSkill(List<TargetData> targets, float rawValue)
    {
        int targetCount = targets.Count;
        for(int i = 0; i < targetCount; i++)
        {
            TriggerSkill(targets[i], rawValue);
        }
    }

    public void TriggerSkill(TargetData targets, float rawValue)
    {
        var iHit = targets.Object.GetComponent<IDamageable>();
        iHit.TakeDamage(rawValue * 1.5f,targets.Coordinates);

        MessageBroker.Default.Publish(new CallVFXSignal
        {
            LifeSpan = 1,
            Position = targets.Coordinates,
            Type = VFXType.FireSlash
        });
    }
}
