﻿using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Heal : GenericSkill,ISkillable
{
    public void TriggerSkill(List<TargetData> targets, float rawValue)
    {
        int targetCount = targets.Count;
        for (int i = 0; i < targetCount; i++)
        {
            TriggerSkill(targets[i], rawValue);
        }
    }

    public void TriggerSkill(TargetData targets, float rawValue)
    {
        var iHit = targets.Object.GetComponent<IDamageable>();
        iHit.Heal(rawValue);
        MessageBroker.Default.Publish(new CallRegenSignal { Damage = rawValue, Lifetime = 1, Position = targets.Object.transform.position });
    }
}
