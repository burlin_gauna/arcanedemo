﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class NormalSlash : GenericSkill, ISkillable
{
    public void TriggerSkill(List<TargetData> targets, float rawValue)
    {
        int targetCount = targets.Count;
        for (int i = 0; i < targetCount; i++)
        {
            TriggerSkill(targets[i], rawValue);
        }
        /*
        MessageBroker.Default.Publish(new CallVFXSignal
        {
            LifeSpan = 1,
            Position = MathClass.GetMidPosition(targets),
            Type = VFXType.ScreenSlash
        });*/
    }

    public void TriggerSkill(TargetData targets, float rawValue)
    {
        var iHit = targets.Object.GetComponent<IDamageable>();
        iHit.TakeDamage(rawValue, targets.Coordinates);

        MessageBroker.Default.Publish(new CallVFXSignal
        {
            LifeSpan = 1,
            Position = targets.Coordinates,
            Type = VFXType.PhysicalSlash
        });
    }
}
